Get Started
-----------
### Install app
```bash
npm install && gulp
```
### Start your server
```bash
npm start
```

### Url application
<http://localhost:3000/>

Changelog
---------

Pour que la génération du changelog se passe bien, il faut respecter le [conventional-changelog](https://github.com/ajoslin/conventional-changelog/blob/master/conventions/angular.md).
Voici la liste des scopes disponibles:

| Scope         | Description                                                                               |
|---------------|-------------------------------------------------------------------------------------------|
| design        | Tout ce qui touche à l'affichage et à l'expérience utilisateur                            |
| config        | Tout ce qui touche à la partie configuration de l'application                             |
| blog          | Tout ce qui touche au blog                                                                |
| typographie   | Internationalisation de l'application et ajout de traduction                              |
| rpm           | Tout ce qui touche à l'outillage pour la construction du RPM (uniquement avec **chore**)  |
| ci            | Tout ce qui touche à l'outillage pour l'intégration continue (uniquement avec **chore**)  |
| gulp          | Tout ce qui concerne gulp (uniquement avec **chore**)                                     |
| cs            | Correction des convention de code (uniquement avec **style**)                             |
